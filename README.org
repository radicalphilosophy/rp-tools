#+TITLE: Setting up the Radical Philosophy site

* Introduction

  This file describes the rp-tools repository. It's purpose is to
  provide a reliable way to perform some basic operations for the
  Radical Philosophy tech project.

  This README file contains the instructions for all of the
  operations.  We then tangle this file into script files that can be
  used to perform those operations manually.
  
** Requirements

   The scripts in this project depend depend on wp-cli
   (https://wp-cli.org/).

   Full list of dependencies:
   - mysql
   - git
   - wp-cli
   - php
   - php-mysql extension

   The RP project currently uses 2 proprietary plugins:
   Advanced-Custom-Fields-Pro and Relevanssi-Premium.

   We consider this technical debt and we have roadmaps to remove
   those.

** Overview

   The RP project is built on Wordpress.  We customize it with our own
   theme (port-au-prince), which in turn makes use of a number of
   plugins.

   This README file is a literate programming file.  You can just
   follow it to perform a number of operations related to Radical
   Philosophy.
   
   We also use it to generate the actual script files that are part of
   this project.

** RP Site Creation
   
   You can use these instructions to generate a new site.
   
*** Create Site

    We should make sure we run the script in a safe mode!

    This will affect your entire shell session, so if you're following
    this script manually just run `bash`, which you can then just `exit`
    at the end of the script.  The only thing you'll have to be aware
    of is that you will need to set the variables according to your
    desired outcome.

    First we set up the safety of our script, & a handy help message.

    #+BEGIN_SRC bash :tangle create-site.sh
      #!/usr/bin/env bash
      set -euf -o pipefail

      target_dbuser="${1:---help}"
      target_dbname="${2:-0}"
      target_dbpass="${3:-0}"
      create_db="${4:-0}"
      source_dbfile="${5:-0}"
      siteurl="${6:-http://localhost}"
      wp_version="${7:-latest}"
      reload_db="${8:-1}"

      if [[ "${target_dbuser}" == "--help" || "${target_dbuser}" == "-h" ]]; then
          echo "create-site.sh TARGET_DBUSER TARGET_DBNAME TARGET_DBPASS CREATE_DB SOURCE_DBFILE SITEURL WP_VERSION RELOAD_DB"
          echo "    TARGET_DBUSER has no default"
          echo "    TARGET_DBNAME has no default"
          echo "    TARGET_DBPASS has no default"
          echo "    CREATE_DB defaults to 0; set to 1 to create TARGET_DB… entries"
          echo "    SOURCE_DBFILE defaults to the gitlab dev-db; use it to point to a sql file"
          echo "    SITEURL defaults to http://localhost"
          echo "    WP_VERSION defaults to latest"
          echo "    RELOAD_DB defaults to 1, which means yes"
          exit
      fi
    #+END_SRC

    Next we want to test that wp-cli is installed where we expect it, &
    that mysql & git are installed.

    #+BEGIN_SRC bash :tangle create-site.sh
      wp --version
      mysql --version
      git --version
    #+END_SRC

    First steps first: we must set up our database environment, if
    this has been requested by the user.  After this, if a db reload
    is requested, which is likely if you're setting up the site for
    the first time, then we'll load that either from our sample db or
    from a db file you specified.

    #+BEGIN_SRC bash :tangle create-site.sh
      if [[ "${create_db}" != 0 ]]; then
          sudo mysql -e "DROP DATABASE IF EXISTS ${target_dbname};                   \
      DROP USER IF EXISTS '${target_dbuser}'@'localhost';                            \
      CREATE DATABASE ${target_dbname};                                              \
      CREATE USER '${target_dbuser}'@'localhost' IDENTIFIED BY '${target_dbpass}';   \
      GRANT ALL PRIVILEGES ON ${target_dbname}.* TO '${target_dbuser}'@'localhost';"
      fi

      if [[ "${reload_db}" == 1 ]]; then
          if [[ "${source_dbfile}" == 0 ]]; then
              curl -O "https://www.radicalphilosophy.com/static/dev_database.tgz"
              gzip -d dev_database.tgz
              mysql -u"${target_dbuser}" -p"${target_dbpass}" "${target_dbname}" < dev_database.sql
              rm dev_database.sql
          else
              mysql -u"${target_dbuser}" -p"${target_dbpass}" "${target_dbname}" < "${source_dbfile}"
          fi
      fi
    #+END_SRC

    Now we can create the directory in which we'll set up the site.

    #+BEGIN_SRC bash :tangle create-site.sh
      mkdir rp-wordpress && cd rp-wordpress
    #+END_SRC

    Next, let's install wordpress, and our plugin
    dependencies.

    #+BEGIN_SRC bash :tangle create-site.sh
      wp core download --version="${wp_version}"                                                                  \
          && wp config create --dbuser="${target_dbuser}" --dbname="${target_dbname}" --dbpass="${target_dbpass}" \
          && wp plugin install --activate add-multiple-users akismet co-authors-plus google-sitemap-generator     \
             nextgen-gallery advanced-custom-fields                                                               \
          && wp db query "UPDATE wp_options SET option_value = '${siteurl}' WHERE option_id ='1';"                \
          && wp db query "UPDATE wp_options SET option_value = '${siteurl}' WHERE option_id ='37';"
    #+END_SRC

    Next up, our port-au-prince theme.  You probably got this script by
    finding our repository, but now we'll set it up in our wordpress
    install:

    #+BEGIN_SRC bash :tangle create-site.sh
      cd wp-content/themes/ &&
          git clone https://gitlab.com/radicalphilosophy/port-au-prince.git &&
          cd ../../
    #+END_SRC

    Finally we need to set up a rudimentary upload folder.  For now the
    upload folder will be empty, so we will have no issue images or PDF
    files.

    #+BEGIN_SRC bash :tangle create-site.sh
      mkdir wp-content/uploads
    #+END_SRC
** RP Updating

   This is a wrapper to update wp core & all plugins installed on a
   site.  You may find this script useful for playing around with your
   local site install of RP — but if you re-generate the site using
   the create-site.sh script you should have the latest version of
   evrything anyway!

*** Update Site

    First we set up safe mode and check that we have wp installed.

    #+BEGIN_SRC bash :tangle update-wp.sh
      #!/usr/bin/env bash
      set -euf -o pipefail

      wp --version
    #+END_SRC
    
    After this we update Core WP.

    #+BEGIN_SRC bash :tangle update-wp.sh
      wp core update && wp core update-db
    #+END_SRC

    Finally we cycle through our installed plugins to update each of
    those.

    #+BEGIN_SRC bash :tangle update-wp.sh
      for p in `wp plugin list --field=name --update=available`; do
          wp plugin update $p
      done
    #+END_SRC
