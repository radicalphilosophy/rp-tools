#!/usr/bin/env bash
set -euf -o pipefail

target_dbuser="${1:---help}"
target_dbname="${2:-0}"
target_dbpass="${3:-0}"
create_db="${4:-0}"
source_dbfile="${5:-0}"
siteurl="${6:-http://localhost}"
wp_version="${7:-latest}"
reload_db="${8:-1}"

if [[ "${target_dbuser}" == "--help" || "${target_dbuser}" == "-h" ]]; then
    echo "create-site.sh TARGET_DBUSER TARGET_DBNAME TARGET_DBPASS CREATE_DB SOURCE_DBFILE SITEURL WP_VERSION RELOAD_DB"
    echo ""
    echo "example: bash create-site.sh localdev devdb devpw 1"
    echo ""
    echo "    TARGET_DBUSER has no default"
    echo "    TARGET_DBNAME has no default"
    echo "    TARGET_DBPASS has no default"
    echo "    CREATE_DB defaults to 0; set to 1 to create TARGET_DB… entries"
    echo "    SOURCE_DBFILE defaults to the gitlab dev-db; use it to point to a sql file"
    echo "    SITEURL defaults to http://localhost"
    echo "    WP_VERSION defaults to latest"
    echo "    RELOAD_DB defaults to 1, which means yes"
    echo "
Use this script to deploy a local rp dev site.  The script should be
run from your www root directory.

TARGET_DBUSER, TARGET_DBNAME & TARGET_DBPASS are the desired, or
existing, username, databes & password of your local dev database.

CREATE_DB instructs the script to create the local database & database
user.
"
    exit
fi

wp --version
mysql --version
git --version
if [ ! -f rp-tools/create-site.sh ]; then
    git clone https://gitlab.com/radicalphilosophy/rp-tools
fi

# Setup database
if [[ "${create_db}" != 0 ]]; then
    sudo mysql -e "DROP DATABASE IF EXISTS ${target_dbname};                   \
DROP USER IF EXISTS '${target_dbuser}'@'localhost';                            \
CREATE DATABASE ${target_dbname};                                              \
CREATE USER '${target_dbuser}'@'localhost' IDENTIFIED BY '${target_dbpass}';   \
GRANT ALL PRIVILEGES ON ${target_dbname}.* TO '${target_dbuser}'@'localhost';"
fi

if [[ "${reload_db}" == 1 ]]; then
    if [[ "${source_dbfile}" == 0 ]]; then
        if [ ! -f dev_database.sql ]; then
            curl -O "https://www.radicalphilosophy.com/static/dev_database.sql.gz"
            gzip -d dev_database.sql.gz
        fi
        mysql -u"${target_dbuser}" -p"${target_dbpass}" "${target_dbname}" < dev_database.sql
    else
        mysql -u"${target_dbuser}" -p"${target_dbpass}" "${target_dbname}" < "${source_dbfile}"
    fi
fi

# Setup wordpress & plugins
mkdir rp-wordpress
cd rp-wordpress

wp core download --version="${wp_version}"
wp config create --dbuser="${target_dbuser}" --dbname="${target_dbname}" --dbpass="${target_dbpass}"
# We have to run plugins-manifest.sh twice to ensure all plugins are
# activated OK.  This seems to be to do with lingering stae in the DB.
./../rp-tools/plugins-manifest.sh
./../rp-tools/plugins-manifest.sh

# Tweak our database for local deployment
wp db query "UPDATE wp_options SET option_value = '${siteurl}' WHERE option_id ='1';"
wp db query "UPDATE wp_options SET option_value = '${siteurl}' WHERE option_id ='37';"

# Install port-au-prince
cd wp-content/themes/
git clone https://gitlab.com/radicalphilosophy/port-au-prince.git
cd port-au-prince
git remote add ssh "git@gitlab.com:radicalphilosophy/port-au-prince.git"
cd ../../../

mkdir wp-content/uploads

# cd back to web root & tidy up.
cd ..
rm -rf rp-tools
rm -f dev_database.sql
