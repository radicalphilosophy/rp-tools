#!/usr/bin/env bash
set -uf -o pipefail

help="${1:-0}"

if [[ "${help}" == "--help" || "${help}" == "-h" ]]; then
    echo "./plugins-manifest.sh [--help]

Use this script to install all plugins used by the radicalphilosophy
project in the current wordpress installation.

The script needs to be called from a wordpress installation.
"
    exit
fi

wp --version

# Uninstall to ensure they are switched off in the database.
wp plugin deactivate --skip-themes         \
   classic-editor                          \
   akismet                                 \
   co-authors-plus                         \
   google-sitemap-generator                \
   advanced-custom-fields
wp plugin uninstall  --skip-themes         \
   classic-editor                          \
   akismet                                 \
   co-authors-plus                         \
   google-sitemap-generator                \
   advanced-custom-fields
rm -r wp-content/plugins/{classic-editor, akismet, co-authors-plus, google-sitemap-generator, advanced-custom-fields}

# Then install cleanly
wp plugin install --activate --skip-themes \
   classic-editor                          \
   akismet                                 \
   co-authors-plus                         \
   google-sitemap-generator                \
   advanced-custom-fields
