#!/usr/bin/env bash
set -euf -o pipefail

wp --version

wp core update && wp core update-db

for p in `wp plugin list --field=name --update=available`; do
    wp plugin update $p
done
